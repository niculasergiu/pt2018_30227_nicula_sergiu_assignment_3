package bll;

import model.*;
import da.*;
import validare.*;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;



public class ClientBLL {

    private List<Validator<Client>> validatorsC;

    public ClientBLL()  {
        validatorsC = new ArrayList<Validator<Client>>();
    }

    public Client findClient(int id) {
        ClientDA a= new ClientDA();
        Client cl = a.findById(id);
        if (cl == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return cl;
    }

    public void deleteClient(int id) {
        ClientDA a= new ClientDA();
        Client cl = a.findById(id);
        if (cl == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        a.delete(id);
    }

    public void insertClient(Client client) throws SQLException, IllegalAccessException {
        for (Validator<Client> v : validatorsC) {
            v.validate(client);
        }
        ClientDA a= new ClientDA();
        a.insert(client);
    }

    public void updateClient(Client client) throws SQLException, IllegalAccessException {
        ClientDA a= new ClientDA();
        Client cl = a.findById(client.getId());
        if (cl == null) {
            throw new NoSuchElementException("The client with id =" + client.getId() + " was not found!");
        }
        a.update(client);
    }

    public List<Client> viewClient() {
        List<Client> cl;
        ClientDA a= new ClientDA();
        cl = a.view();
        if (cl == null) {
            throw new NoSuchElementException("The client was not found!");
        }
        return cl;
    }
}
