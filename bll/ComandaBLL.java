package bll;

import da.ComandaDA;
import model.Comanda;
import validare.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ComandaBLL {

    private List<Validator<Comanda>> validatorsCo;

    public ComandaBLL() {
        validatorsCo = new ArrayList<Validator<Comanda>>();
    }

    public void deleteComandaidC(int id) {
        List<Comanda> co = ComandaDA.viewIdC(id);
        if (co == null) {
            throw new NoSuchElementException("The comanda with idClient =" + id + " was not found!");
        }
        ComandaDA a = new ComandaDA();
        a.deleteIdC(id);
    }

    public List<Comanda> viewIdComanda(int idC) {
        List<Comanda> co = ComandaDA.viewIdC(idC);
        if (co == null) {
            throw new NoSuchElementException("The comanda was not found!");
        }
        return co;
    }

    public Comanda findComanda(int id) {
        ComandaDA a= new ComandaDA();
        Comanda co = a.findById(id);
        if (co == null) {
            throw new NoSuchElementException("The comanda with id =" + id + " was not found!");
        }
        return co;
    }

    public void deleteComanda(int id) {
        ComandaDA a= new ComandaDA();
        Comanda co = a.findById(id);
        if (co == null) {
            throw new NoSuchElementException("The comanda with id =" + id + " was not found!");
        }
        a.delete(id);
    }

    public void insertComanda(Comanda comanda) throws SQLException, IllegalAccessException {
        for (Validator<Comanda> v : validatorsCo) {
            v.validate(comanda);
        }
        ComandaDA a= new ComandaDA();
        a.insert(comanda);
    }

    public void updateComanda(Comanda comanda) throws SQLException, IllegalAccessException {
        ComandaDA a= new ComandaDA();
        Comanda co = a.findById(comanda.getId());
        if (co == null) {
            throw new NoSuchElementException("The comanda with id =" + comanda.getId() + " was not found!");
        }
        a.update(comanda);
    }

    public List<Comanda> viewComanda() {
        List<Comanda> co;
        ComandaDA a= new ComandaDA();
        co = a.view();
        if (co == null) {
            throw new NoSuchElementException("The comanda was not found!");
        }
        return co;
    }

}
