package bll;

import da.ProducatorDA;
import model.Comanda;
import model.Producator;
import validare.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProducatorBLL {

    private List<Validator<Producator>> validatorsPr;

    public ProducatorBLL() {
        validatorsPr = new ArrayList<Validator<Producator>>();
    }

    public Producator findProducator(int id) {
        ProducatorDA a= new ProducatorDA();
        Producator pr = a.findById(id);
        if (pr == null) {
            throw new NoSuchElementException("The producator with id =" + id + " was not found!");
        }
        return pr;
    }

    public void deleteProducator(int id) {
        ProducatorDA a= new ProducatorDA();
        Producator pr = a.findById(id);
        if (pr == null) {
            throw new NoSuchElementException("The producator with id =" + id + " was not found!");
        }
        a.delete(id);
    }

    public void insertProducator(Producator producator) throws SQLException, IllegalAccessException {
        for (Validator<Producator> v : validatorsPr) {
            v.validate(producator);
        }
        ProducatorDA a= new ProducatorDA();
        a.insert(producator);
    }

    public void updateProducator(Producator producator) throws SQLException, IllegalAccessException {
        ProducatorDA a= new ProducatorDA();
        Producator pr = a.findById(producator.getId());
        if (pr == null) {
            throw new NoSuchElementException("The producator with id =" + producator.getId() + " was not found!");
        }
        a.update(producator);
    }

    public List<Producator> viewProducator() {
        List<Producator> pr;
        ProducatorDA a= new ProducatorDA();
        pr = a.view();
        if (pr == null) {
            throw new NoSuchElementException("The producator was not found!");
        }
        return pr;
    }

}
