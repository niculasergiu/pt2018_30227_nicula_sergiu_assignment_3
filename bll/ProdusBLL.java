package bll;

import da.ProdusDA;
import model.Comanda;
import model.Produs;
import validare.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProdusBLL {

    private List<Validator<Produs>> validatorsP;

    public ProdusBLL() {
        validatorsP = new ArrayList<Validator<Produs>>();
    }

    public Produs findProdus(int id) {
        ProdusDA a= new ProdusDA();
        Produs p = a.findById(id);
        if (p == null) {
            throw new NoSuchElementException("The produs with id =" + id + " was not found!");
        }
        return p;
    }

    public void deleteProdus(int id) {
        ProdusDA a= new ProdusDA();
        Produs p = a.findById(id);
        if (p == null) {
            throw new NoSuchElementException("The produs with id =" + id + " was not found!");
        }
        a.delete(id);
    }

    public void insertProdus(Produs produs) throws SQLException, IllegalAccessException {
        for (Validator<Produs> v : validatorsP) {
            v.validate(produs);
        }
        ProdusDA a= new ProdusDA();
        a.insert(produs);
    }

    public void updateProdus(Produs produs) throws SQLException, IllegalAccessException {
        ProdusDA a= new ProdusDA();
        Produs p = a.findById(produs.getId());
        if (p == null) {
            throw new NoSuchElementException("The produs with id =" + produs.getId() + " was not found!");
        }
        a.update(produs);
    }

    public List<Produs> viewProdus() {
        List<Produs> p;
        ProdusDA a= new ProdusDA();
        p = a.view();
        if (p == null) {
            throw new NoSuchElementException("The produs was not found!");
        }
        return p;
    }


}