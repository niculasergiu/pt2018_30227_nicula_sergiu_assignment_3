package da;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import conectare.*;

public class AbsDA<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbsDA.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbsDA() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String deleteSelectQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + "id" + " =?");
        return sb.toString();
    }

    private String createInsertQuery(Vector<String> field){
        StringBuilder sb = new StringBuilder();
        sb.append(" INSERT ");
        sb.append(" INTO ");
        sb.append(type.getSimpleName());
        sb.append("(");
        for(int i=1;i<field.size();i++){
            sb.append(field.get(i));
            if(i!=(field.size()-1)){
                sb.append(",");
            }
        }

        sb.append(")");
        sb.append(" VALUES ");
        sb.append("(");
        for(int i=1;i<field.size();i++){
            sb.append("?");
            if(i!=(field.size()-1)){
                sb.append(",");
            }
        }
        sb.append(")");
        return sb.toString();
    }
    private String createAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    private String createUpdateQuery(Vector<String> field) {
        StringBuilder sb = new StringBuilder();
        sb.append(" UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        for(int i=1;i<field.size();i++){
            sb.append(field.get(i)+" = ?");
            if(i!=(field.size()-1)){
                sb.append(",");
            }
        }
        sb.append(" WHERE " + field.get(0) + " =?");
        return sb.toString();
    }



    public List<T> view() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createAllQuery();
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DA:view " + e.getMessage());
        } finally {
            Conexiune.close(resultSet);
            Conexiune.close(statement);
            Conexiune.close(connection);
        }
        return null;
    }

    public   T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            Conexiune.close(resultSet);
            Conexiune.close(statement);
            Conexiune.close(connection);
        }
        return null;
    }

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insert(T t) throws IllegalAccessException, SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        Vector<String> capTabel = new Vector<String>();
        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                    capTabel.add(field.getName());

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        String query = createInsertQuery(capTabel);
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            int ok=0;
            for(Field field : t.getClass().getDeclaredFields()){
                if(ok==1) {
                    field.setAccessible(true);
                    if (field.get(t) instanceof Integer) {
                        statement.setInt(i, (Integer) field.get(t));
                        i++;
                    } else {
                        statement.setString(i, field.get(t) + "");
                        i++;
                    }
                }
                else{
                    ok=1;
                }
            }
            statement.executeUpdate();
            statement.getGeneratedKeys();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DA:insert " + e.getMessage());
        } finally {
            Conexiune.close(statement);
            Conexiune.close(connection);
        }
    }

    public void update(T t) throws IllegalAccessException, SQLException  {
        Connection connection = null;
        PreparedStatement statement = null;
        Vector<String> capTabel = new Vector<String>();
        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                capTabel.add(field.getName());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        String query = createUpdateQuery(capTabel);
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            int ok=0;
            for(Field field : t.getClass().getDeclaredFields()){
                if(ok==1) {
                    field.setAccessible(true);
                    if (field.get(t) instanceof Integer) {
                        statement.setInt(i, (Integer) field.get(t));
                        i++;
                    } else {
                        statement.setString(i, field.get(t) + "");
                        i++;
                    }
                }
                else{
                    field.setAccessible(true);
                    statement.setObject(capTabel.size(), (Integer) field.get(t));
                    ok=1;
                }
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DA:update " + e.getMessage());
        } finally {
            Conexiune.close(statement);
            Conexiune.close(connection);
        }
    }

    public void delete(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        String query = deleteSelectQuery();
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ComandaDA:delete " + e.getMessage());
        } finally {
            Conexiune.close(statement);
            Conexiune.close(connection);
        }
    }
}

