package da;
import model.*;
import conectare.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;


public class ComandaDA extends AbsDA<Comanda> {

    protected static final Logger LOGGER = Logger.getLogger(ComandaDA.class.getName());
    private final static String viewCStatementString = "SELECT * FROM comanda where idClient = ?";
    private final static String deleteIdCStatementString = "DELETE FROM comanda where idClient = ?";


    public static List<Comanda> viewIdC(int idc) {
        List<Comanda> toReturn = new ArrayList<>() ;
        Connection dbConnection = Conexiune.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(viewCStatementString);
            findStatement.setLong(1, idc);
            rs = findStatement.executeQuery();
            while(rs!=null) {
                rs.next();
                int id = rs.getInt("id");
                int idProdus = rs.getInt("idProdus");
                int cant = rs.getInt("cant");
                Comanda co = new Comanda(id, idc, idProdus, cant);
                toReturn.add(co);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDA:find " + e.getMessage());
        } finally {
            Conexiune.close(rs);
            Conexiune.close(findStatement);
            Conexiune.close(dbConnection);
        }
        return toReturn;
    }

    public static void deleteIdC(int id) {
        Connection dbConnection = Conexiune.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteIdCStatementString);
            deleteStatement.setLong(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ComandaDA:delete " + e.getMessage());
        } finally {
            Conexiune.close(deleteStatement);
            Conexiune.close(dbConnection);
        }
    }
}