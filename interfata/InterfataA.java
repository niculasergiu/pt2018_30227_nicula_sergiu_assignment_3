package interfata;
import da.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InterfataA extends JFrame implements ActionListener {

    private JButton factura = new JButton("FACTURA");
    private JButton client = new JButton("CLIENT");
    private JButton producator = new JButton("PRODUCATOR");
    private JButton produs = new JButton("PRODUS");
    private JLabel scris = new JLabel("ADMIN");

    public InterfataA(){}

    public InterfataA(String title){
        setTitle(title);
        setSize(400,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);

        factura.setBounds(100,150,200,60);
        producator.setBounds(100,250,200,60);
        client.setBounds(100,350,200,60);
        produs.setBounds(100,450,200,60);

        scris.setBounds(0,50,400,60);

        Font buton = new Font("", Font.BOLD, 18);
        Font mesaj = new Font("", Font.BOLD, 25);

        producator.setHorizontalAlignment(JTextField.CENTER);
        producator.setFont(buton);
        client.setHorizontalAlignment(JTextField.CENTER);
        client.setFont(buton);
        produs.setHorizontalAlignment(JTextField.CENTER);
        produs.setFont(buton);
        factura.setHorizontalAlignment(JTextField.CENTER);
        factura.setFont(buton);
        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        panel.add(producator);
        panel.add(client);
        panel.add(scris);
        panel.add(produs);
        panel.add(factura);

        factura.addActionListener(new InterfataFac());
        client.addActionListener(new InterfataClient());
        producator.addActionListener(new InterfataProducator());
        produs.addActionListener(new InterfataProdus());


    }
    public void actionPerformed (ActionEvent e){
        new InterfataA ("ADMIN");
    }
}


