package interfata;
import da.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InterfataC extends JFrame implements ActionListener {

    private JButton produs = new JButton("PRODUS");
    private JButton comanda = new JButton("COMANDA");
    private JLabel scris = new JLabel("CLIENT");

    public InterfataC(){}

    public InterfataC(String title){
        setTitle(title);
        setSize(400,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);

        produs.setBounds(100,250,200,60);
        comanda.setBounds(100,350,200,60);
        scris.setBounds(0,100,400,60);

        Font buton = new Font("", Font.BOLD, 18);
        Font mesaj = new Font("", Font.BOLD, 25);

        produs.setHorizontalAlignment(JTextField.CENTER);
        produs.setFont(buton);
        comanda.setHorizontalAlignment(JTextField.CENTER);
        comanda.setFont(buton);
        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);


        panel.add(produs);
        panel.add(comanda);
        panel.add(scris);

        comanda.addActionListener(new InterfataComanda());
        produs.addActionListener( new InterfataTabel(2));

    }
    public void actionPerformed (ActionEvent e){
        new InterfataC ("CLIENT");
    }
}


