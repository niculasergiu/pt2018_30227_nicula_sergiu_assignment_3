package interfata;

import da.*;
import bll.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

public class InterfataComanda extends JFrame implements ActionListener {

    private JLabel scris = new JLabel("COMANDA");
    private JTextField idClientT = new JTextField("idClient");
    private JTextField idProdusT = new JTextField("idProdus");
    private JTextField cantitateT = new JTextField("cantitate");

    JButton insert = new JButton("INSERT");

    public InterfataComanda(){}

    public InterfataComanda(String title){
        setTitle(title);
        setSize(600,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);


        scris.setBounds(0,50,600,60);


        idClientT.setBounds(100,220,100,40);
        idProdusT.setBounds(100,290,100,40);
        cantitateT.setBounds(100,360,100,40);

        insert.setBounds(300,250,200,60);



        Font mesaj = new Font("", Font.BOLD, 25);
        Font txt = new Font("", Font.BOLD, 12);
        Font buton = new Font("", Font.BOLD, 18);

        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);


        idClientT.setHorizontalAlignment(JLabel.CENTER);
        idClientT.setFont(txt);
        idProdusT.setHorizontalAlignment(JLabel.CENTER);
        idProdusT.setFont(txt);
        cantitateT.setHorizontalAlignment(JLabel.CENTER);
        cantitateT.setFont(txt);

        insert.setHorizontalAlignment(JLabel.CENTER);
        insert.setFont(buton);

        panel.add(scris);

        panel.add(idClientT);
        panel.add(idProdusT);
        panel.add(cantitateT);

        panel.add(insert);

        insert.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                ProdusBLL a = new ProdusBLL();
                ComandaBLL b = new ComandaBLL();
                Produs produs = a.findProdus(Integer.parseInt(idProdusT.getText()));
                if(produs.getCant()>=Integer.parseInt(cantitateT.getText())) {
                    Produs up = new Produs(produs.getId(),produs.getNume(), (produs.getCant()-Integer.parseInt(cantitateT.getText())), produs.getPret(), produs.getIdProducator());
                    Comanda comanda = new Comanda(Integer.parseInt(idClientT.getText()), Integer.parseInt(idProdusT.getText()), Integer.parseInt(cantitateT.getText()));
                    try {
                        a.updateProdus(up);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        b.insertComanda(comanda);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }
    public void actionPerformed (ActionEvent e){ new InterfataComanda ("COMANDA");
    }
}


