package interfata;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import da.*;
import model.*;
import javax.swing.*;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import bll.*;

import com.itextpdf.text.Document;

public class InterfataFac extends JFrame implements ActionListener {

    private JButton creaza = new JButton("CREAZA FACTURA");
    private JButton view = new JButton("VIEW COMANDA");
    private JTextField id = new JTextField("");
    private JLabel scris = new JLabel("FACTURA");
    private JLabel idC = new JLabel("ClientID");
    private static int nr=0;

    public InterfataFac(){}

    public InterfataFac(String title){
        setTitle(title);
        setSize(600,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);

        scris.setBounds(0,50,600,60);
        id.setBounds(250,200,200,60);
        idC.setBounds(80,200,200,60);
        creaza.setBounds(200,300,200,60);
        view.setBounds(200,400,200,60);

        Font buton = new Font("", Font.BOLD, 18);
        Font mesaj = new Font("", Font.BOLD, 25);

        id.setHorizontalAlignment(JTextField.CENTER);
        id.setFont(buton);
        idC.setHorizontalAlignment(JTextField.CENTER);
        idC.setFont(buton);
        creaza.setHorizontalAlignment(JTextField.CENTER);
        creaza.setFont(buton);
        view.setHorizontalAlignment(JTextField.CENTER);
        view.setFont(buton);
        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        panel.add(id);
        panel.add(idC);
        panel.add(creaza);
        panel.add(scris);
        panel.add(view);

        creaza.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)

            {
                nr++;
                int crt=1;
                int sumaT=0;
                String inf =" ";
                ComandaBLL comanda = new ComandaBLL();
                ClientBLL client = new ClientBLL();
                ProdusBLL produs = new ProdusBLL();
                List<Comanda> c=  comanda.viewIdComanda(Integer.parseInt(id.getText()));
                Client cli=client.findClient(Integer.parseInt(id.getText()));
                List<Produs> pr = new ArrayList<>();
                for(int i=0;i<c.size();i++){
                    Produs p= produs.findProdus(c.get(i).getIdProdus());
                    pr.add(p);
                }
                inf=inf+cli.toString();
                inf=inf+"                                                           FACTURA FISCALA\n\n";
                inf=inf+"                                                                   Factura numarul: "+nr+"\n\n\n\n";
                inf=inf+"Nr     Denumire produs                    Cant                Pret                Valoare\n\n";
                for(int i=0;i<c.size();i++){
                inf=inf+""+crt+"        "+pr.get(i).getNume()+"                                             "+c.get(i).getCant()+"                   "+pr.get(i).getPret()+"                        "+(c.get(i).getCant()*pr.get(i).getPret())+"\n";
                sumaT=sumaT+(c.get(i).getCant()*pr.get(i).getPret());
                crt++;
                }
                inf=inf+"\n\n\n";
                inf=inf+"                                                                                SUMA TOTALA DE PLATA: "+sumaT;
                inf=inf+"\n\n\n                                              MULTUMIM PENTRU ALEGERE!!!!!!!";
                Document d = new Document();
                try {
                    PdfWriter.getInstance(d, new FileOutputStream("FACTURA.pdf"));
                    d.open();
                    Paragraph p = new Paragraph();
                    p.add(inf);
                    d.add(p);
                    d.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            comanda.deleteComandaidC(cli.getId());
            }

        });
        view.addActionListener( new InterfataTabel(3));
    }

    public void actionPerformed (ActionEvent e){
        new InterfataFac ("FACTURA");
    }
}



