package interfata;
import da.*;
import model.*;
import javax.swing.*;
import java.awt.*;

public class InterfataP extends JFrame {

    private JButton client = new JButton("CLIENT");
    private JButton admin = new JButton("ADMIN");
    private JLabel scris = new JLabel("DEPOZIT");

    public InterfataP(String title){
        setTitle(title);
        setSize(400,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);


        client.setBounds(100,250,200,60);
        admin.setBounds(100,350,200,60);
        scris.setBounds(0,100,400,60);

        Font buton = new Font("", Font.BOLD, 18);
        Font mesaj = new Font("", Font.BOLD, 25);

        client.setHorizontalAlignment(JTextField.CENTER);
        client.setFont(buton);
        admin.setHorizontalAlignment(JTextField.CENTER);
        admin.setFont(buton);
        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        panel.add(client);
        panel.add(admin);
        panel.add(scris);

        client.addActionListener(new InterfataC());
        admin.addActionListener(new InterfataA());


    }
    public static void main(String[] args) {
        new InterfataP ("Depozit");
    }

}


