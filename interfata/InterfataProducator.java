package interfata;
import da.*;
import bll.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.List;

public class InterfataProducator extends JFrame implements ActionListener {

    private JLabel scris = new JLabel("PRODUCATOR");
    private JTextField idT = new JTextField("id");
    private JTextField numeT = new JTextField("nume");
    private JTextField domeniulT = new JTextField("domeniul");
    private JTextField adresaT = new JTextField("adresa");
    private JTextField emailT = new JTextField("email");

    private JButton insert = new JButton("INSERT");
    private JButton delete = new JButton("DELETE");
    private JButton update = new JButton("UPDATE");
    private JButton view = new JButton("VIEW");

    public InterfataProducator(){}

    public InterfataProducator(String title){
        setTitle(title);
        setSize(600,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);


        scris.setBounds(0,50,600,60);

        idT.setBounds(100,150,100,40);
        numeT.setBounds(100,220,100,40);
        domeniulT.setBounds(100,290,100,40);
        adresaT.setBounds(100,360,100,40);
        emailT.setBounds(100,430,100,40);

        insert.setBounds(300,175,200,60);
        delete.setBounds(300,250,200,60);
        update.setBounds(300,325,200,60);
        view.setBounds(300,400,200,60);



        Font mesaj = new Font("", Font.BOLD, 25);
        Font txt = new Font("", Font.BOLD, 12);
        Font buton = new Font("", Font.BOLD, 18);

        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        idT.setHorizontalAlignment(JLabel.CENTER);
        idT.setFont(txt);
        numeT.setHorizontalAlignment(JLabel.CENTER);
        numeT.setFont(txt);
        domeniulT.setHorizontalAlignment(JLabel.CENTER);
        domeniulT.setFont(txt);
        adresaT.setHorizontalAlignment(JLabel.CENTER);
        adresaT.setFont(txt);
        emailT.setHorizontalAlignment(JLabel.CENTER);
        emailT.setFont(txt);

        insert.setHorizontalAlignment(JLabel.CENTER);
        insert.setFont(buton);
        delete.setHorizontalAlignment(JLabel.CENTER);
        delete.setFont(buton);
        update.setHorizontalAlignment(JLabel.CENTER);
        update.setFont(buton);
        view.setHorizontalAlignment(JLabel.CENTER);
        view.setFont(buton);

        panel.add(scris);

        panel.add(idT);
        panel.add(numeT);
        panel.add(domeniulT);
        panel.add(adresaT);
        panel.add(emailT);

        panel.add(insert);
        panel.add(delete);
        panel.add(update);
        panel.add(view);

        insert.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                Producator producator = new Producator(numeT.getText(), domeniulT.getText(), adresaT.getText(), emailT.getText());
                ProducatorBLL a = new ProducatorBLL();
                try {
                    a.insertProducator(producator);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            }
        });
        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                ProducatorBLL a = new ProducatorBLL();
                a.deleteProducator(Integer.parseInt(idT.getText()));
            }
        });
        update.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                Producator producator = new Producator(Integer.parseInt(idT.getText()), numeT.getText(), domeniulT.getText(), adresaT.getText(), emailT.getText());
                ProducatorBLL a = new ProducatorBLL();
                try {
                    a.updateProducator(producator);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            }
        });
        view.addActionListener( new InterfataTabel(4));
    }
    public void actionPerformed (ActionEvent e){
        new InterfataProducator ("PRODUCATOR");
    }
}


