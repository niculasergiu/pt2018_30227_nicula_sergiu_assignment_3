package interfata;
import da.*;
import bll.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.List;

public class InterfataProdus extends JFrame implements ActionListener {

    private JLabel scris = new JLabel("PRODUS");
    private JTextField idT = new JTextField("id");
    private JTextField numeT = new JTextField("nume");
    private JTextField cantitateT = new JTextField("cantitate");
    private JTextField pretT = new JTextField("pret");
    private JTextField idProducatorT = new JTextField("idProducator");

    private JButton insert = new JButton("INSERT");
    private JButton delete = new JButton("DELETE");
    private JButton update = new JButton("UPDATE");
    private JButton view = new JButton("VIEW");

    public InterfataProdus(){}

    public InterfataProdus(String title){
        setTitle(title);
        setSize(600,600);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea
        panel.setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setContentPane(panel);


        scris.setBounds(0,50,600,60);

        idT.setBounds(100,150,100,40);
        numeT.setBounds(100,220,100,40);
        cantitateT.setBounds(100,290,100,40);
        pretT.setBounds(100,360,100,40);
        idProducatorT.setBounds(100,430,100,40);

        insert.setBounds(300,175,200,60);
        delete.setBounds(300,250,200,60);
        update.setBounds(300,325,200,60);
        view.setBounds(300,400,200,60);



        Font mesaj = new Font("", Font.BOLD, 25);
        Font txt = new Font("", Font.BOLD, 12);
        Font buton = new Font("", Font.BOLD, 18);

        scris.setHorizontalAlignment(JLabel.CENTER);
        scris.setFont(mesaj);

        idT.setHorizontalAlignment(JLabel.CENTER);
        idT.setFont(txt);
        numeT.setHorizontalAlignment(JLabel.CENTER);
        numeT.setFont(txt);
        cantitateT.setHorizontalAlignment(JLabel.CENTER);
        cantitateT.setFont(txt);
        pretT.setHorizontalAlignment(JLabel.CENTER);
        pretT.setFont(txt);
        idProducatorT.setHorizontalAlignment(JLabel.CENTER);
        idProducatorT.setFont(txt);

        insert.setHorizontalAlignment(JLabel.CENTER);
        insert.setFont(buton);
        delete.setHorizontalAlignment(JLabel.CENTER);
        delete.setFont(buton);
        update.setHorizontalAlignment(JLabel.CENTER);
        update.setFont(buton);
        view.setHorizontalAlignment(JLabel.CENTER);
        view.setFont(buton);

        panel.add(scris);

        panel.add(idT);
        panel.add(numeT);
        panel.add(cantitateT);
        panel.add(pretT);
        panel.add(idProducatorT);

        panel.add(insert);
        panel.add(delete);
        panel.add(update);
        panel.add(view);

        insert.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                Produs produs = new Produs(numeT.getText(), Integer.parseInt(cantitateT.getText()), Integer.parseInt(pretT.getText()), Integer.parseInt(idProducatorT.getText()));
                ProdusBLL a = new ProdusBLL();
                try {
                    a.insertProdus(produs);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            }
        });
        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                ProdusBLL a = new ProdusBLL();
                a.deleteProdus(Integer.parseInt(idT.getText()));
            }
        });
        update.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {

                Produs produs = new Produs(Integer.parseInt(idT.getText()),numeT.getText(), Integer.parseInt(cantitateT.getText()), Integer.parseInt(pretT.getText()), Integer.parseInt(idProducatorT.getText()));
                ProdusBLL a = new ProdusBLL();
                try {
                    a.updateProdus(produs);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            }
        });
        view.addActionListener( new InterfataTabel(2));
    }
    public void actionPerformed (ActionEvent e){
        new InterfataProdus ("PRODUS");
    }
}


