package interfata;
import da.*;
import model.*;
import javax.swing.*;
import java.awt.*;
import bll.*;
import java.awt.event.*;
import java.util.List;
import java.util.Vector;

public class InterfataTabel extends JFrame implements ActionListener {
    private  JTable tabel;
    private static int f;

    public InterfataTabel(int f){
        this.f=f;
    }
    public InterfataTabel(String title) {
        Vector<Object> ob = new Vector<Object>();
        if(f==1) {
            ClientBLL b = new ClientBLL();
            List<Client> client;
            client = b.viewClient();
            for(Object a : client ){
                ob.add(a);
            }
        }
        if(f==2) {
            ProdusBLL b = new ProdusBLL();
            List<Produs> produs;
            produs = b.viewProdus();
            for(Object a : produs ){
                ob.add(a);
            }
        }
        if(f==3) {
            ComandaBLL b = new ComandaBLL();
            List<Comanda> comanda;
            comanda = b.viewComanda();
            for(Object a : comanda ){
                ob.add(a);
            }
        }
        if(f==4) {
            ProducatorBLL b = new ProducatorBLL();
            List<Producator> producator;
            producator = b.viewProducator();
            for(Object a : producator ){
                ob.add(a);
            }
        }
        tabel = ReflectionT.createTable(ob);
        setTitle(title);
        setSize(800, 800);//dimensiune interfata
        setResizable(false);
        JPanel panel = new JPanel();//fcream un obiect de tip JPanel
        panel.setLayout(null);

        panel.setEnabled(true);          ///sa il putem folosii si vedea

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel);

        JScrollPane js = new JScrollPane(tabel);
        js.setBounds(100, 50, 600, 600);
        setVisible(true);
        panel.add(js);
    }
    public void actionPerformed(ActionEvent e) {
        new InterfataTabel("TABEL");
    }
}
