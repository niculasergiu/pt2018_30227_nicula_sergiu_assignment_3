package model;

import java.util.*;
public class Client {

    private int id;
    private String nume;
    private String adresa;
    private String CNP;
    private String email;

    public Client(){}

    public Client(int id, String nume, String adresa, String CNP, String email) {
        this.id = id;
        this.nume = nume;
        this.adresa = adresa;
        this.CNP = CNP;
        this.email = email;
    }

    public Client(String nume, String adresa, String CNP, String email) {
        this.nume = nume;
        this.adresa = adresa;
        this.CNP = CNP;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "    CLIENT " +
                "\nid : " + id +
                "\nnume : " + nume +
                "\nadresa : " + adresa +
                "\nCNP : " + CNP +
                "\nemail : " + email+"\n\n\n";
    }
}
