package model;

public class Comanda {
    private int id;
    private int idClient;
    private int idProdus;
    private int cant;

    public Comanda(int id, int idClient, int idProdus, int cant) {
        this.id = id;
        this.idClient = idClient;
        this.idProdus = idProdus;
        this.cant = cant;
    }
    public Comanda(){}
    public Comanda(int idClient, int idProdus, int cant) {
        this.idClient = idClient;
        this.idProdus = idProdus;
        this.cant = cant;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "id=" + id +
                ", idClient=" + idClient +
                ", idProdus=" + idProdus +
                ", cant=" + cant +
                '}';
    }
}
