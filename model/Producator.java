package model;

public class Producator {
    private int id;
    private String nume;
    private String domeniul;
    private String adresa;
    private String email;

    public Producator(int id, String nume, String domeniul, String adresa, String email) {
        this.id = id;
        this.nume = nume;
        this.domeniul = domeniul;
        this.adresa = adresa;
        this.email = email;
    }

    public Producator(){}
    public Producator(String nume, String domeniul, String adresa, String email) {
        this.nume = nume;
        this.domeniul = domeniul;
        this.adresa = adresa;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getDomeniul() {
        return domeniul;
    }

    public void setDomeniul(String domeniul) {
        this.domeniul = domeniul;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Producator{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", domeniul='" + domeniul + '\'' +
                ", adresa='" + adresa + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
