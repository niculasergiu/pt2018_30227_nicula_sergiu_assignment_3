package model;

public class Produs {
    private int id;
    private String nume;
    private int cant;
    private int pret;
    private int idProducator;

    public Produs(int id, String nume, int cant, int pret, int idProducator) {
        this.id = id;
        this.nume = nume;
        this.cant = cant;
        this.pret = pret;
        this.idProducator = idProducator;
    }
    public Produs(){}
    public Produs(String nume, int cant, int pret, int idProducator) {
        this.nume = nume;
        this.cant = cant;
        this.pret = pret;
        this.idProducator = idProducator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public int getIdProducator() {
        return idProducator;
    }

    public void setIdProducator(int idProducator) {
        this.idProducator = idProducator;
    }

    @Override
    public String toString() {
        return "Produs{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", cant=" + cant +
                ", pret=" + pret +
                ", idProducator=" + idProducator +
                '}';
    }
}
