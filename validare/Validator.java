package validare;

import model.Client;

public interface Validator<T> {

    public void validate(T t);
}